package goethe

import (
	"github.com/gin-gonic/gin"
	"github.com/golang-jwt/jwt/v4"
)

const (
	ENDPOINT_NAME       = "Chk"
	HEADER_SERVICE_NAME = "X-SERVICE-NAME"
	HEADER_SERVICE_KEY  = "X-SERVICE-KEY"
	HEADER_ACCESS_KEY   = "X-ACCESS-KEY"
)

type GCallbacks struct {
	Secret              []byte
	BeforeIssueJWT      func(c *gin.Context, claims *jwt.MapClaims) error
	OnVerifyCredentials func(name, key []byte) GOETHE_AUTH_LEVEL
	OnVerifyClaims      func(c *gin.Context, claims *jwt.MapClaims) error
}

var Callbacks *GCallbacks = nil

func GoetheEntry() *gin.Engine {
	if Callbacks == nil {
		panic("goethe: Callbacks must be set before calling GoetheEntry()")
	}

	e := gin.Default()

	eGroup := e.Group(ENDPOINT_NAME)
	{
		/**
		* 1. client goes to Chk/init with serviceName (or with both serviceName and serviceKey) header
		*  There he obtains the AccessKey
		* 2. Using service key and access key, he goes to Chk/authorize (if didn't provided key in step 1)
		*  There he gets new AccessKey
		* 3. Client sends the AccessKey via header on any other request, it's verified and it holds the authorization info
		* 4. Endpoints responds with context.MustGet("JWT").(*goethe.GClaims).NewResponse(...), which creates JSON data for the response
		 */

		eGroup.GET("/init", Init)
		eGroup.GET("/authorize", Authorize)
		// keep alive endpoint
		//eGroup.GET("/keepalive", KeepAlive)
	}

	e.Use(LoadAuthLevel)

	return e
}
