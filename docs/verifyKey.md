# Example Verify Key

You can use goethe.model.Credentials

```go
const (
	MIN_NAME_LEN = 5
	MIN_KEY_LEN  = 8
)

// VerifyService verifies the service name and key.
//
// Parameters:
//
//	name: Service name as a byte array.
//	key: Service key as a byte array.
//
// Returns:
//
//	Authentication status as an integer.
func VerifyNameKey(name, key []byte) goethe.GOETHE_AUTH_LEVEL {
	if len(name) < MIN_NAME_LEN {
		return goethe.AUTH_NONE
	}

	if DB == nil {
		return goethe.AUTH_NONE
	}

	// find service by name
	var credentials gm.Credentials
	DB.First(&credentials, "name = ? AND valid_until > NOW()", name)

	if (credentials.Name == "") || (credentials.Key == "") {
		return goethe.AUTH_NONE
	}

	now := time.Now()

	// update last access
	credentials.LastAccess = &now

	// save last access
	DB.Save(&credentials)

	// no key, but found the service
	if len(key) == 0 {
		// service key not set
		return goethe.AUTH_SERVICE_NAME
	}

	// wrong key length
	if len(key) < MIN_KEY_LEN {
		return goethe.AUTH_SERVICE_WRONG_KEY
	}

	// compare service key with hashed value in database
	err := bcrypt.CompareHashAndPassword([]byte(credentials.Key), key)
	if err != nil {
		return goethe.AUTH_SERVICE_WRONG_KEY
	}

	return goethe.AUTH_SERVICE_KEY
}
```