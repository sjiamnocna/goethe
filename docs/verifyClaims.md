# Example implementation of a custom claim verifier

```go
// VerifyClaims verifies control claims contained in a JWT token
func VerifyClaims(c *gin.Context, claims *jwt.MapClaims) (*goethe.GClaims, error) {
	if c == nil || claims == nil {
		log.Println("claims or context is nil")
		return nil, nil
	}

	// check the expiration
	if claims.Valid() != nil {
		return nil, errors.New("token expired")
	}

    // TODO: implement Database storage for sessions

	// save claims for later use
	return &GClaims{Claims: claims, C: c}, nil
}
```