package goethe

import (
	"crypto/rand"
	"encoding/hex"
)

// GenerateHexString generates a hexadecimal string of the given length.
// If the length is odd, the resulting string length will be the closest lower even number.
//
// Parameters:
//
//	len: Byte length of the resulting string.
//
// Returns:
//
//	A hexadecimal string.
func GenerateHexString(len byte) string {
	// Create a byte slice to hold the random bytes, resulting string will be twice as long
	randomBytes := make([]byte, len/2)

	// Read random bytes into the byte slice
	_, err := rand.Read(randomBytes)

	if err != nil {
		return ""
	}

	// Convert the random bytes to a hexadecimal string
	return hex.EncodeToString(randomBytes)
}
