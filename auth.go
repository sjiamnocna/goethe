package goethe

import (
	"fmt"
	"net/http"
	"time"

	"errors"
	"log"

	"github.com/gin-gonic/gin"
	"github.com/golang-jwt/jwt/v4"
)

const TokenValidity = time.Hour * 3

type GOETHE_AUTH_LEVEL uint8

const (
	AUTH_NONE              GOETHE_AUTH_LEVEL = iota // none authentication
	AUTH_SERVICE_NAME                               // valid service name provided
	AUTH_SERVICE_WRONG_KEY                          // service name and key, but key wrong
	AUTH_SERVICE_KEY                                // both service name and key were valid
	AUTH_USER_LOGGED_IN                             // user is logged in
)

// Initialize service with name and key or just with name, creates new JWT token, assigns authLevel to context
func Init(c *gin.Context) {
	// get service name and key from headers
	serviceName := c.GetHeader(HEADER_SERVICE_NAME)
	serviceKey := c.GetHeader(HEADER_SERVICE_KEY)

	// service name must be provided
	if serviceName == "" {
		c.AbortWithError(http.StatusUnauthorized, errors.New("service name was not provided"))
		return
	}

	// verify service name and key with database and get permission level
	authLevel := Callbacks.OnVerifyCredentials([]byte(serviceName), []byte(serviceKey))

	switch GOETHE_AUTH_LEVEL(authLevel) {
	case AUTH_NONE, AUTH_SERVICE_WRONG_KEY:
		c.AbortWithError(http.StatusUnauthorized, fmt.Errorf("init: service name (%s) or key (%s) is invalid (%d)\n", serviceName, serviceKey, authLevel))
		return
	}

	// claims saved in JWT
	Claims := jwt.MapClaims{
		"sub":       serviceName,
		"authLevel": authLevel,
		"exp":       time.Now().Add(TokenValidity).Unix(),
	}

	// allow developer to verifi custom claims
	if err := Callbacks.BeforeIssueJWT(c, &Claims); err != nil {
		c.AbortWithError(http.StatusInternalServerError, err)
		return
	}

	// generate access key
	if token, err := IssueJWT(&Claims); err != nil {
		c.AbortWithError(http.StatusInternalServerError, err)
		return
	} else {
		c.JSON(http.StatusOK, ResponseData{
			[]string{
				"accessKey",
			},
			map[string]string{
				"accessKey": token,
			},
			http.StatusOK,
		})
	}
}

// Authorize service with key if JWT was already issued, reissues the token, assigns authLevel to context
func Authorize(c *gin.Context) {
	accessKey := c.GetHeader(HEADER_ACCESS_KEY)
	serviceKey := c.GetHeader(HEADER_SERVICE_KEY)

	if accessKey == "" || serviceKey == "" {
		log.Printf("authorize: access key (%s) or service key (%s) was not provided\n", accessKey, serviceKey)
		c.AbortWithStatus(http.StatusUnauthorized)
		return
	}

	token, err := VerifyJWT(c, accessKey)
	if err != nil {
		c.AbortWithError(http.StatusUnauthorized, err)
		return
	}

	if claims, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
		// let developer to verify claims
		if err := Callbacks.OnVerifyClaims(c, &claims); err != nil {
			c.AbortWithError(http.StatusBadRequest, err)
			return
		}

		// verify service name and key with database and get permission level
		authLevel := Callbacks.OnVerifyCredentials([]byte(claims["sub"].(string)), []byte(serviceKey))

		switch authLevel {
		case AUTH_NONE, AUTH_SERVICE_WRONG_KEY:
			c.AbortWithError(http.StatusUnauthorized, fmt.Errorf("authlevel: service name (%s) or key (%s) is invalid (%d)\n", claims["sub"].(string), serviceKey, authLevel))
			return
		}

		JWT := c.MustGet("JWT").(*GClaims)

		JWT.Claims["exp"] = time.Now().Add(TokenValidity).Unix()
		JWT.Claims["authLevel"] = authLevel

		// update the service token
		c.JSON(http.StatusOK, ResponseData{
			nil,
			nil,
			http.StatusOK,
		})
	} else {
		c.AbortWithError(http.StatusUnauthorized, fmt.Errorf("authorize: JWT claims error (%s)\n", err.Error()))
	}
}

// Validate JWT token, GIN middleware
func LoadAuthLevel(c *gin.Context) {
	accessKey := c.GetHeader(HEADER_ACCESS_KEY)
	serviceName := c.GetHeader(HEADER_SERVICE_NAME)
	serviceKey := c.GetHeader(HEADER_SERVICE_KEY)

	if serviceName != "" || serviceKey != "" {
		// don't proceed as it's init
		return
	}

	if accessKey == "" {
		c.AbortWithError(http.StatusBadRequest, errors.New("access key not provided for loading auth level (have: '')"))
		return
	}

	token, err := VerifyJWT(c, accessKey)
	if err != nil {
		c.AbortWithError(http.StatusUnauthorized, err)
		return
	}

	if claims, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
		// let developer to verify claims
		if err := Callbacks.OnVerifyClaims(c, &claims); err != nil {
			c.AbortWithError(http.StatusBadRequest, fmt.Errorf("LoadAuthLevel: JWT.Valid: %s\n", err.Error()))
			return
		}

		// verify service name and key with database and get permission level
		authLevel := GOETHE_AUTH_LEVEL(claims["authLevel"].(float64))

		if GOETHE_AUTH_LEVEL(authLevel) == AUTH_NONE {
			c.AbortWithError(http.StatusUnauthorized, errors.New("service is not authorized (authLevel == AUTH_NONE)"))
			return
		}

		if claims["user"] != nil {
			// TODO: check with DB that user session is ok
			authLevel = AUTH_USER_LOGGED_IN
		}

		claims["authLevel"] = authLevel

		newClaims := GClaims{
			NeedReclaim: false,
			Claims:      claims,
		}

		// initialize JWT values after check
		c.Set("JWT", &newClaims)
	} else {
		// anything else results in unauthorized
		c.AbortWithError(http.StatusUnauthorized, errors.New("JWT claims error"))
	}

	c.Next()
}

/*
  - Generates middleware function to require authLevel
    *
  - Usage:
  - gWithMiddleware := engine.Group("/RequiresAuthLevel", requireAuthLevel(goethe.AUTH_SERVICE_KEY))
  - {
  - endpoint_groups.RegisterAppGroup(appGroup)
  - }
*/
func RequireAuthLevel(level GOETHE_AUTH_LEVEL) gin.HandlerFunc {
	return func(ctx *gin.Context) {
		authLevel := int(ctx.MustGet("JWT").(*GClaims).Claims["authLevel"].(float64))
		if GOETHE_AUTH_LEVEL(authLevel) < level {
			ctx.AbortWithError(http.StatusUnauthorized, errors.New("authlevel is not sufficient"))
			return
		}
	}
}
