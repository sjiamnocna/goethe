package goethe

import "net/http"

/**
 * ResponseData is the response data structure for the API
 * compatible with gitlab.com/sjiamnocna/renette-api
 */

type ResponseData struct {
	Action []string    `json:"action"`
	Data   interface{} `json:"data"`
	Code   int         `json:"code"`
}

// NewResponse creates a new ResponseData object
//
// action: the action that was performed
// data: the data to be sent
// code: the HTTP status code
//
// returns: a pointer to a ResponseData object
func (G *GClaims) NewResponse(action []string, data map[string]interface{}, code int) *ResponseData {
	if code == 0 {
		code = http.StatusOK
	}

	if G.NeedReclaim {
		// replace token with new one
		accessKey, err := G.ReclaimJWT()
		if err == nil {

			action = append(action, "accessKey")
			data["accessKey"] = accessKey
		} else {
			// add error to response
			action = append(action, "accessKeyError")
			data["accessKeyError"] = err.Error()
		}
	}

	return &ResponseData{
		Action: action,
		Data:   data,
		Code:   code,
	}
}
