package goethe

import (
	"errors"
	"fmt"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/golang-jwt/jwt/v4"
)

// allows us to define custom actions on jwt.MapClaims
type GClaims struct {
	NeedReclaim bool
	Claims      jwt.MapClaims
}

func IssueJWT(claims *jwt.MapClaims) (string, error) {
	// generate a JWT token
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)

	// Sign and get the complete encoded token as a string
	tokenString, err := token.SignedString(Callbacks.Secret)

	if err != nil {
		return "", err
	}

	return tokenString, nil
}

func VerifyJWT(c *gin.Context, tokenString string) (*jwt.Token, error) {
	token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		// check signing method
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, jwt.ErrInvalidKey
		}

		// hmacSampleSecret is a []byte containing secret to decrypt the token, e.g. []byte("my_secret_key")
		return Callbacks.Secret, nil
	})

	if err != nil {
		return nil, err
	}

	claims := token.Claims.(jwt.MapClaims)

	if err != nil {
		return nil, err
	}

	// allow to verify and modify claims
	if err := Callbacks.OnVerifyClaims(c, &claims); err != nil {
		return nil, fmt.Errorf("something went wrong with the token %v", err)
	}

	// add context into JWT
	VerifiedClaims := GClaims{
		NeedReclaim: false,
		Claims:      claims,
	}
	// save pointer as context var
	c.Set("JWT", &VerifiedClaims)

	return token, nil
}

func (JWT *GClaims) ReclaimJWT() (string, error) {
	if JWT.Claims == nil {
		return "", errors.New("claims are nil")
	}

	// update the claims
	JWT.Claims["exp"] = time.Now().Add(TokenValidity).Unix()

	// reissue the token
	token, error := IssueJWT(&JWT.Claims)

	if error != nil {
		e := errors.New("failed to reissue token")
		return "", e
	}
	return token, nil
}

/** Set JWT claim and mark it for reclaim */
func (JWT *GClaims) SetClaim(key string, val interface{}) {
	// set for reclaim
	JWT.NeedReclaim = true
	JWT.Claims[key] = val
}

func (JWT *GClaims) RemoveClaim(key string) {
	// set for reclaim
	JWT.NeedReclaim = true
	delete(JWT.Claims, key)
}
